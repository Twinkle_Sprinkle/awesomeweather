package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        city=findViewById(R.id.editText);
        temp=findViewById(R.id.textView);
        feelsLike = findViewById(R.id.textView2);
        windSpeed = findViewById(R.id.textView3);
    }
    EditText city;
    TextView temp, feelsLike, windSpeed;
    public  void City(){
        String url = "https://api.openweathermap.org/data/2.5/weather?q=" + city.getText().toString() + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric";
        JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject main = response.getJSONObject("main");
                    JSONObject Wind = response.getJSONObject("wind");

                    String temp2 = String.valueOf(main.getInt("temp"));
                    String feelsLike_J = String.valueOf(main.getInt("feels_like"));
                    String windSpeed_J = String.valueOf(Wind.getInt("speed"));

                    temp.setText( "Температура: " + temp2.toString());
                    feelsLike.setText( "Ощущается как: " + feelsLike_J.toString());
                    windSpeed.setText( "Скорость ветра: " + windSpeed_J.toString() + " м/с");

                } catch (JSONException e) {temp.setText(e.getMessage()); } }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { }});
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jo);
    }
    public void click(View view) {
        City();
    }
}
